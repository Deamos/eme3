#Pyton Imports
import sys
import uuid
import datetime
import requests
import json
import random
from operator import itemgetter
import os

#basedir = os.path.abspath(os.path.dirname(__file__))

#Import Paths
cwp = sys.path[0]
sys.path.append(cwp)
sys.path.append('./class')
sys.path.append('./functions')

#Config Import
from config import *


#Import Flask
from flask import Flask, render_template
from flask import request, redirect, session, url_for, flash
from flask_sqlalchemy import SQLAlchemy

import uuid

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = dbLocation

db = SQLAlchemy(app)

#db.init_app(app)
db.text_factory = str

# Flask App Config
app.secret_key = str(uuid.uuid4())
app.config['PERMANENT_SESSION_LIFETIME']=1440091
app.config['SESSION_REFRESH_EACH_REQUEST']=True
app.config['SESSION_COOKIE_SECURE'] = False

#Function Import
from functions import crestAuth
#from functions import crestGet
from functions import esiGet
from functions import sde
from functions import xmlAPIGet
from functions import criusIndustry
from functions import checkCitadels

#Class Import
from classes import invTypes
from classes import pilot
from classes import accountBalance
from classes import assetList
from classes import blueprints
from classes import industryJobs
from classes import marketOrders
from classes import package
from classes import linkedManufacturing
from classes import linkedMarket
from classes import mapSolarSystems
from classes import mapRegions
from classes import staStations
from classes import knownCitadel
from classes import marketHistoryData
from classes import completedPackage
from classes import packageMaterials

# DB Config
db.create_all()

#######################################################################
# End Import
#######################################################################

sdeData = sde.readSDE()
sde.readMapCSV()


# Filter Statements
def format_hms(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    d,h = divmod(h,24)
    return '%03d:%02d:%02d:%02d' % (d,h, m, s)


def getTimeRemaining(timeValue):
    timeDelta = timeValue - datetime.datetime.utcnow()

    timeDeltaInSec = timeDelta.total_seconds()

    return timeDeltaInSec


def format_currency(value):
    value = float(value)
    return "{:,.2f}".format(value)


def genRandomColor():
    r = lambda: random.randint(0, 255)
    color = '#%02X%02X%02X' % (r(), r(), r())
    return color


def typeIDtoTypeName(typeID):
    type = esiGet.getTypeIDInfo(typeID)
    if type != None:
        return type.name
    else:
        return "Unknown"


def characterIDtoCharacterName(charID):
    characterData = esiGet.getCharacterData(charID)

    if characterData != None:
        return characterData['name']
    else:
        return "Unknown"


def getCurrentPricesFilter(typeID):
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    currentUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'],accessToken=session['auth_data']['access_token']).first()
    prices = None
    if currentUserQuery.defaultPriceLocationType == 1:
        prices = esiGet.getMarketPrice('solarSystem',currentUserQuery.defaultPriceLocationID,typeID)
    else:
        prices = esiGet.getMarketPrice('region', currentUserQuery.defaultPriceLocationID, typeID)

    price = prices[currentUserQuery.defaultPriceCalcType][currentUserQuery.defaultPriceCalcHighLow]
    return price


def getStationName(stationID):
    stationName = "Unknown"

    # Check if item exists in a Station Pilot Hangar
    stationQuery = staStations.staStations.query.filter_by(stationID=long(stationID)).first()
    if stationQuery != None:
        stationName = stationQuery.stationName
    else:
        # Check if item exists in a Citadel Pilot Hangar
        stationQuery = checkCitadels.get_Citadel(long(stationID), session['auth_data']['access_token'])
        if stationQuery != None:
            stationName = stationQuery.name
        else:
            # Check if Item is in a Can
            assetQuery = assetList.assetList.query.filter_by(charID=session['auth_user']['id'], itemID=long(stationID)).first()
            if assetQuery != None:
                # Check to see if in Station Hangar
                stationQuery = staStations.staStations.query.filter_by(stationID=long(assetQuery.locationID)).first()
                if stationQuery != None:
                    stationName = stationQuery.stationName
                else:
                    # Check to see if in Citadel Hangar
                    stationQuery = checkCitadels.get_Citadel(long(assetQuery.locationID), session['auth_data']['access_token'])
                    if stationQuery != None:
                        stationName = stationQuery.name
                    else:
                        # Check to see if in an Office Location
                        locationID = long(assetQuery.locationID)
                        if locationID >= 66000000 and locationID < 67000000:
                            locationID = locationID - 6000001
                        elif locationID >= 67000000 and locationID < 68000000:
                            locationID = locationID - 6000000
                        stationName = solarSystemIDtoSolarSystemName(stationIDtoSolarSystemID(locationID, ittr=True))

            else:
                # Check to see if in an Office Location
                locationID = long(assetQuery.locationID)
                if locationID >= 66000000 and locationID < 67000000:
                    locationID = locationID - 6000001
                elif locationID >= 67000000 and locationID < 68000000:
                    locationID = locationID - 6000000
                stationName = solarSystemIDtoSolarSystemName(stationIDtoSolarSystemID(locationID, ittr=True))
                if stationName == "Unknown":
                    stationQuery = checkCitadels.get_Citadel(long(locationID), session['auth_data']['access_token'])
                    if stationQuery != None:
                        stationName = stationQuery.name
    return stationName


def solarSystemIDtoSolarSystemName(systemID):
    if systemID == 999 or systemID == None:
        return "Unknown"
    else:
        system = mapSolarSystems.mapSolarSystems.query.filter_by(solarSystemID=int(systemID)).first()
    return system.solarSystemName


def stationIDtoSolarSystemID(stationID, ittr=False):
    solarSystemID = None
    stationQuery = staStations.staStations.query.filter_by(stationID=long(stationID)).first()
    if stationQuery != None:
        solarSystemID = stationQuery.solarSystemID
    else:
        stationQuery = knownCitadel.knownCitadel.query.filter_by(citadelID=long(stationID)).first()
        if stationQuery != None:
            solarSystemID = stationQuery.solarSystemID
        else:
            assetQuery = assetList.assetList.query.filter_by(charID=session['auth_user']['id'], itemID=long(stationID)).first()
            if assetQuery != None:
                stationQuery = knownCitadel.knownCitadel.query.filter_by(citadelID=long(assetQuery.locationID)).first()
                if stationQuery != None:
                    solarSystemID = stationQuery.solarSystemID
                else:
                    stationQuery = checkCitadels.get_Citadel(long(stationID), session['auth_data']['access_token'])
                    if stationQuery != None:
                        solarSystemID = stationQuery.solarSystemID
                    else:
                        if assetQuery != None:
                            locationID = long(assetQuery.locationID)
                            if locationID >= 66000000 and locationID < 67000000:
                                locationID = locationID - 6000001
                            elif locationID >= 67000000 and locationID < 68000000:
                                locationID = locationID - 6000000
                            solarSystemID = stationIDtoSolarSystemID(locationID,ittr=True)
    return solarSystemID

app.jinja_env.filters['hms_format'] = format_hms
app.jinja_env.filters['currency_format'] = format_currency
app.jinja_env.filters['typeIDtoTypeName'] = typeIDtoTypeName
app.jinja_env.filters['systemIDtoSystemName'] = solarSystemIDtoSolarSystemName
app.jinja_env.filters['stationIDtoSolarSystemID'] = stationIDtoSolarSystemID
app.jinja_env.filters['getCurrentPricesFilter'] = getCurrentPricesFilter
app.jinja_env.filters['getStationName'] = getStationName
app.jinja_env.filters['characterIDtoCharacterName'] = characterIDtoCharacterName
app.jinja_env.filters['getTimeRemaining'] = getTimeRemaining


################################################################################################
@app.context_processor
def inject_dict_for_all_templates():
    return dict(sessionData=session)


@app.context_processor
def generateRandomColor():
    def _generateRandomColor():
        r = lambda: random.randint(0, 255)
        color = '#%02X%02X%02X' % (r(), r(), r())
        return color
    return dict(generateRandomColor=_generateRandomColor)


@app.context_processor
def getSellPriceforSystem():
    def _getSalePriceforSystem(typeID,systemID):
        prices = esiGet.getMarketPrice('solarSystem', systemID, typeID)
        price = prices['sell']['low']
        return price
    return dict(getSellPriceforSystem=_getSalePriceforSystem)


@app.route('/')
def main_page():
    char_count = pilot.pilot.query.count()
    package_count = package.package.query.count()
    return render_template('index.html',version=str(version), package_count=package_count, char_count=char_count)


@app.route('/login')
def login_Crest_Redirect():
    state = str(uuid.uuid4())
    session['auth_state'] = state
    redirectURL = crestAuth.get_auth_URL(client_id,state)
    return redirect(redirectURL)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('main_page'))


@app.route('/options')
def options_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    regions = mapRegions.mapRegions.query.order_by(mapRegions.mapRegions.regionName)
    solarSystems = mapSolarSystems.mapSolarSystems.query.order_by(mapSolarSystems.mapSolarSystems.solarSystemName)
    existingUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'], accessToken=session['auth_data']['access_token']).first()
    page = None
    page = request.args.get('page')
    if page == None:
        page = "buysell"

    return render_template('options.html', currentUser=existingUserQuery, regions=regions, solarSystems=solarSystems, page=page)


@app.route('/options/change',methods=['POST'])
def options_change_function():
    existingUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'], accessToken=session['auth_data']['access_token']).first()
    page = request.form['page']

    if page == 'buysell':
        marketPriceCalcLocationType = int(request.form['marketPriceCalcLocationType'])

        marketPriceCalcLocation = 0
        if marketPriceCalcLocationType == 0:
            marketPriceCalcLocation = int(request.form['marketPriceCalcLocationRegion'])

        elif marketPriceCalcLocationType == 1:
            marketPriceCalcLocation = int(request.form['marketPriceCalcLocationSystem'])

        if marketPriceCalcLocation == 0:
            marketPriceCalcLocation = 30000142

        marketPriceCalcOrderType = request.form['marketPriceCalcOrderType']
        marketPriceCalcHighLow = request.form['marketPriceCalcHighLow']

        existingUserQuery.defaultPriceCalcType = marketPriceCalcOrderType
        existingUserQuery.defaultPriceCalcHighLow = marketPriceCalcHighLow
        existingUserQuery.defaultPriceLocationType = marketPriceCalcLocationType
        existingUserQuery.defaultPriceLocationID = marketPriceCalcLocation
        db.session.commit()
    if page == 'manufacturing':
        defaultBuildLocation = int(request.form['defaultBuildLocation'])
        existingUserQuery.defaultBuildLocation = defaultBuildLocation
        db.session.commit()

    return redirect('/options?page=' + page)


@app.route('/load/<page>')
def loadingPage(page):
    return render_template('loading.html', URL=url_for(page))


@app.route('/crestAuth/')
def login_Crest_Auth_Code():

    code = request.args.get('code')
    state = request.args.get('state')

    if state != session['auth_state']:
        return redirect(url_for('main_page'))

    else:
        if session.has_key == "auth_data":
            existingUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id']).first()

            access_token_json = crestAuth.get_access_token(client_id, secret_key, existingUserQuery.refreshToken, "refresh")
            character_data_json = crestAuth.get_auth_character(access_token_json['access_token'])

            existingUserQuery.updateToken(access_token_json['access_token'], access_token_json['refresh_token'], access_token_json['expires_in'])

            db.session.commit()

            session['auth_user'] = character_data_json
            session['auth_data'] = access_token_json

        else:
            access_token_json = crestAuth.get_access_token(client_id, secret_key, code, "auth")
            character_data_json = crestAuth.get_auth_character(access_token_json['access_token'])

            session['auth_user'] = character_data_json
            session['auth_data'] = access_token_json

            existingUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id']).first()
            if existingUserQuery == None:
                newPilot = pilot.pilot(session['auth_user']['id'], access_token_json['access_token'], access_token_json['token_type'], access_token_json['refresh_token'], int(access_token_json['expires_in']))
                db.session.add(newPilot)
                db.session.commit()
            else:
                existingUserQuery.updateToken(session['auth_data']['access_token'], access_token_json['refresh_token'], session['auth_data']['expires_in'])
                db.session.commit()

        return redirect(url_for('loadingPage', page='refresh_API_Data'))


@app.route('/refreshData/')
def refresh_API_Data():

    refresh_XML_accountBalance()
    refresh_XML_assetList()
    #refresh_XML_blueprints()
    refreshESIBlueprints()
    refresh_XML_industryJobs()
    refresh_XML_marketOrders()
    refresh_XML_walletJournal()

    return redirect(url_for('main_page'))


@app.route('/packages/list/')
def package_list_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))
    packageListQuery = package.package.query.filter_by(charID=session['auth_user']['id']).all()

    totalCost = 0
    totalExpectedProfit = 0
    totalCurrentProfit = 0

    timeRemaining = {}
    marketData = {}

    jobsList = {}

    packageMaterialsList = {}

    for packageItem in packageListQuery:

        totalCost = totalCost + packageItem.componentCost + packageItem.sysBuildCosts
        totalExpectedProfit = totalExpectedProfit + (packageItem.producedItemPrice-(packageItem.componentCost + packageItem.sysBuildCosts + (packageItem.sysBuildCosts*packageItem.tax) + (packageItem.producedItemPrice*packageItem.salesTax)))
        totalCurrentProfit = totalCurrentProfit + ((getCurrentPricesFilter(packageItem.typeIDProduced) * packageItem.producedAmount)-(packageItem.componentCost+packageItem.sysBuildCosts + (packageItem.sysBuildCosts*packageItem.tax)+(getCurrentPricesFilter(packageItem.typeIDProduced)*packageItem.salesTax)))
        longestJob = None
        if packageItem.stage >= 3 and packageItem.stage < 5:
            linkedJobs = linkedManufacturing.linkedManufacturing.query.filter_by(linkedPackage=packageItem.packageID).all()
            for job in linkedJobs:
                jobData = industryJobs.industryJobs.query.filter_by(charID=session['auth_user']['id'], jobID=job.industryJobID).first()
                currentTime = datetime.datetime.utcnow()
                if longestJob == None:
                    if jobData != None:
                        longestJob = jobData.endDate - currentTime
                    else:
                        longestJob = datetime.timedelta(0, 0)
                else:
                    if jobData == None:
                        longestJob = datetime.timedelta(0, 0)
                    else:
                        deltaTime = jobData.endDate - currentTime
                        if longestJob < deltaTime:
                            longestJob = deltaTime
            if longestJob == None:
                timeRemaining[packageItem.packageID] = 0
            else:
                timeRemaining[packageItem.packageID] =  int(longestJob.total_seconds())
        if packageItem.stage == 5:
            marketData[packageItem.packageID] = {}
            linkedMarketData = linkedMarket.linkedMarket.query.filter_by(charID=session['auth_user']['id'], linkedPackage=packageItem.packageID).all()
            totalEntered = 0
            totalRemaining = 0
            lowestPrice = 9999999999999999
            for item in linkedMarketData:
                marketQuery = marketOrders.marketOrders.query.filter_by(charID=session['auth_user']['id'], orderID=item.marketID).first()
                if marketQuery != None:
                    if marketQuery.price < lowestPrice:
                        lowestPrice = marketQuery.price
                    totalEntered = totalEntered + marketQuery.volEntered
                    totalRemaining = totalRemaining + marketQuery.volRemaining
            marketData[packageItem.packageID]['totalEntered'] = totalEntered
            marketData[packageItem.packageID]['totalRemaining'] = totalRemaining
            marketData[packageItem.packageID]['lowestPrice'] = lowestPrice

        packageMaterialsList[packageItem.packageID] = packageMaterials.packageMaterials.query.filter_by(packageID=packageItem.packageID).all()

        for job in packageItem.linkedJobs:
            jobQuery = industryJobs.industryJobs.query.filter_by(jobID=job.industryJobID).first()
            jobsList[job] = jobQuery

    timeDelta = datetime.timedelta(seconds=1)

    return render_template('/packagelist.html', packageList=packageListQuery, totalCost=totalCost, totalExpectedProfit=totalExpectedProfit, totalCurrentProfit=totalCurrentProfit,marketData=marketData,
                           timeRemaining=timeRemaining, timeDelta=timeDelta, packageMaterialsList=packageMaterialsList, jobsList=jobsList)


@app.route('/packages/delete')
def package_delete_funct():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    packageID = str(request.args.get('packageID'))
    selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
    if selectedPackage.stage >= 2:
        linkedJobs = linkedManufacturing.linkedManufacturing.query.filter_by(charID=session['auth_user']['id'],linkedPackage=packageID).all()
        for job in linkedJobs:
            db.session.delete(job)
    db.session.delete(selectedPackage)

    materialsList = packageMaterials.packageMaterials.query.filter_by(packageID=packageID).all()
    for item in materialsList:
        db.session.delete(item)
    db.session.commit()
    return redirect(url_for('package_list_page'))


@app.route('/packages/change')
def package_change_funct():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    packageID = str(request.args.get('packageID'))
    option = str(request.args.get('option'))

    if option == 'setToAwaitManufacture':
        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'],packageID=packageID).first()
        selectedPackage.stage = 1
        db.session.commit()
    elif option == 'setToManufacturing':
        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
        selectedPackage.stage = 2
        db.session.commit()
    elif option == 'setToSelling':
        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
        selectedPackage.stage = 4
        db.session.commit()

    elif option == 'linkPackage':

        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
        refresh_XML_industryJobs()

        # Retrieve Linkable Jobs
        possibleJobs = industryJobs.industryJobs.query.filter_by(charID=session['auth_user']['id'], blueprintTypeID=int(selectedPackage.blueprintDataID), activityID=1)

        # Check to see if a Job has already been linked
        linkedJobs = []
        for job in selectedPackage.linkedJobs:
            linkedJobs.append(job.industryJobID)

        currentTime = datetime.datetime.utcnow()

        return render_template('manufacturingpackagelink.html', jobs=possibleJobs, package=selectedPackage, linkedJobs=linkedJobs, currentTime=currentTime)

    elif option == 'deletePackageLink':
        jobID = int(request.args.get('jobID'))
        selectedLink = linkedManufacturing.linkedManufacturing.query.filter_by(charID=session['auth_user']['id'], industryJobID=jobID, linkedPackage=packageID).first()

        db.session.delete(selectedLink)
        db.session.commit()

    elif option == 'linkMarket':
        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
        refresh_XML_marketOrders()
        possibleOrders = marketOrders.marketOrders.query.filter_by(charID=session['auth_user']['id'], typeID=selectedPackage.typeIDProduced, bid=False)
        return render_template('marketpackagelink.html', orders=possibleOrders, package=selectedPackage)

    elif option == 'completePackage':
        selectedPackage = package.package.query.filter_by(charID=session['auth_user']['id'], packageID=packageID).first()
        endDate = datetime.datetime.utcnow()

        linkedMarket = []
        for order in selectedPackage.linkedMarket:
            linkedMarket.append(order.marketID)

        saleISK = 0
        for id in linkedMarket:
            orderQuery = marketOrders.marketOrders.query.filter_by(orderID=long(id))
            saleISK = saleISK + (orderQuery.volEntered * orderQuery.price)

        estProfit = saleISK - (selectedPackage.componentCost + selectedPackage.componentCost)

        newCompletePackage = completedPackage.completedPackage(packageID, session['auth_user']['id'], selectedPackage.blueprintDataID, selectedPackage.blueprintME, selectedPackage.blueprintTE, selectedPackage.runs, selectedPackage.producedAmount,
                                                               selectedPackage.typeIDProduced, selectedPackage.sysID, selectedPackage.sysIndex, selectedPackage.facilityMEModifier, selectedPackage.facilityTEModifier, selectedPackage.tax,
                                                               selectedPackage.sysBuildCosts, selectedPackage.componentCost, selectedPackage.producedItemPrice, estProfit, selectedPackage.startDate, endDate)
        db.session.add(newCompletePackage)
        db.session.delete(selectedPackage)
        db.session.commit()

    return redirect(url_for('package_list_page'))


@app.route('/packages/link',methods=['POST'])
def packagelist_link():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    linkType = str(request.form['linkType'])
    packageID = str(request.form['packageID'])

    if linkType == 'industry':

        jobIDs = request.form.getlist('jobIDs')

        for item in jobIDs:
            job = linkedManufacturing.linkedManufacturing(session['auth_user']['id'], long(item), packageID)
            db.session.add(job)

        if jobIDs != []:
            changedPackage = package.package.query.filter_by(packageID=packageID).first()
            changedPackage.stage = 3
            db.session.commit()

    elif linkType == 'market':
        orderIDs = request.form.getlist('orderIDs')

        for item in orderIDs:
            order = linkedMarket.linkedMarket(session['auth_user']['id'], long(item), packageID)
            db.session.add(order)

        if orderIDs != []:
            changedPackage = package.package.query.filter_by(packageID=packageID).first()
            changedPackage.stage = 5
            db.session.commit()

    return redirect(url_for('package_list_page'))


@app.route('/packages/new/')
def package_new_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    #refresh_XML_blueprints()
    refreshESIBlueprints()

    ownedBlueprints = blueprints.blueprints.query.filter(blueprints.blueprints.charID==session['auth_user']['id']).order_by(blueprints.blueprints.typeID).all()
    solarSystems = mapSolarSystems.mapSolarSystems.query.order_by(mapSolarSystems.mapSolarSystems.solarSystemName)

    builtPackage = {}
    selectedBP = 0

    action = request.args.get('action')
    blueprintID = request.args.get('blueprintID')

    currentUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'], accessToken=session['auth_data']['access_token']).first()

    defaultSolarSystemID = currentUserQuery.defaultBuildLocation

    if action == 'createPackage':
        runs = request.args.get('runs')
        mFacilityModifier = float(request.args.get('mFacilityModifier'))
        tFacilityModifier = float(request.args.get('tFacilityModifier'))
        industrySkill = int(request.args.get('industrySkill'))
        advIndustrySkill = int(request.args.get('advIndustrySkill'))
        tImplantModifier = float(request.args.get('tImplantModifier'))
        stationTax = float(request.args.get('stationTax'))/100
        solarSystemID = int(request.args.get('solarSystemID'))
        skills = {'industry':industrySkill,'advIndustry':advIndustrySkill}
        tSkillModifier = (1-(skills['industry']*.04)) * (1-(skills['advIndustry']*.03))
        tImplantModifiers=tImplantModifier
        salesTax = .03

        selectedBlueprint = blueprints.blueprints.query.filter_by(itemID=long(blueprintID)).first()

        builtPackage = packageBuild(selectedBlueprint.typeID, selectedBlueprint.materialEfficiency, selectedBlueprint.timeEfficiency, runs, solarSystemID, mFacilityModifier, tFacilityModifier, tSkillModifier, tImplantModifiers, stationTax, salesTax)
        newPackage = package.package(session['auth_user']['id'], builtPackage['bpTypeID'], blueprintID, runs, builtPackage['products'][0]['quantity'], selectedBlueprint.timeEfficiency, selectedBlueprint.materialEfficiency, builtPackage['products'][0]['typeID'],
                                     builtPackage['systemID'], builtPackage['systemIndex'], builtPackage['facilityMEModifier'], builtPackage['facilityTEModifier'], builtPackage['stationTax'], builtPackage['totalFees'], builtPackage['totalMaterialCost'],
                                     builtPackage['totalSalePrice'], builtPackage['expectedProfit'], salesTax)
        db.session.add(newPackage)
        db.session.commit()

        blueprintData = sdeData['blueprints'][int(selectedBlueprint.typeID)]['activities']
        materialModifier = criusIndustry.getMaterialModifier(float(1 - (float(selectedBlueprint.materialEfficiency) / 100)), float(builtPackage['facilityMEModifier']))

        defaultBuyLocation = currentUserQuery.defaultPriceLocationID
        defaultBuyLocationType = None
        if currentUserQuery.defaultPriceLocationType == 0:
            defaultBuyLocationType = 'region'
        else:
            defaultBuyLocationType = 'solarSystem'

        for item in blueprintData['manufacturing']['materials']:
            realMaterialRequirement = (criusIndustry.getProductionRequiredMaterials(int(runs), item['quantity'], materialModifier))

            materialPricePer = esiGet.getMarketPrice(defaultBuyLocationType, defaultBuyLocation, item['typeID'],accessToken=session['auth_data']['access_token'])[currentUserQuery.defaultPriceCalcType][currentUserQuery.defaultPriceCalcHighLow]
            requirementCost = realMaterialRequirement * materialPricePer

            newPackageMaterials = packageMaterials.packageMaterials(newPackage.packageID,item['typeID'],realMaterialRequirement,materialPricePer)
            db.session.add(newPackageMaterials)
            db.session.commit()

        return redirect(url_for('package_list_page'))

    if action == None and blueprintID == None:

        skills = {'industry': 5, 'advIndustry': 5}
        tImplantModifiers = 1
        tSkillModifier = (1-(skills['industry'] * .04)) * (1-(skills['advIndustry'] * .03))

        if ownedBlueprints == []:
            flash("No Blueprint Identified in API")
            return redirect(url_for('main_page'))

        builtPackage = packageBuild(ownedBlueprints[0].typeID, ownedBlueprints[0].materialEfficiency, ownedBlueprints[0].timeEfficiency, 1, defaultSolarSystemID, 1,1, tSkillModifier, tImplantModifiers, .10, .03)
        selectedBP = ownedBlueprints[0].itemID

    elif action == None or action == 'refresh' and blueprintID != None:

        if 'runs' in request.args:
            runs = request.args.get('runs')
        else:
            runs = 1
        if 'mFacilityModifier' in request.args:
            mFacilityModifier = float(request.args.get('mFacilityModifier'))
        else:
            mFacilityModifier = 1.0

        #tT2SkillModifier = request.args.get('tT2SkillModifier')
        #tT1SkillModifier = request.args.get('tT1SkillModifier')

        if 'tFacilityModifier' in request.args:
            tFacilityModifier = float(request.args.get('tFacilityModifier'))
        else:
            tFacilityModifier = 1.0

        if 'industrySkill' in request.args:
            industrySkill = int(request.args.get('industrySkill'))
        else:
            industrySkill = 5

        if 'advIndustrySkill' in request.args:
            advIndustrySkill = int(request.args.get('advIndustrySkill'))
        else:
            advIndustrySkill = 5

        if 'tImplantModifier' in request.args:
            tImplantModifier = float(request.args.get('tImplantModifier'))
        else:
            tImplantModifier =  1.0

        if 'stationTax' in request.args:
            stationTax = float(request.args.get('stationTax'))/100
        else:
            stationTax = .10

        if 'solarSystemID' in request.args:
            solarSystemID = int(request.args.get('solarSystemID'))
        else:
            solarSystemID = defaultSolarSystemID

        skills = {'industry':industrySkill,'advIndustry':advIndustrySkill}
        tSkillModifier = (1-(skills['industry']*.04)) * (1-(skills['advIndustry']*.03))
        tImplantModifiers=tImplantModifier

        selectedBlueprint = blueprints.blueprints.query.filter_by(itemID=long(blueprintID)).first()

        builtPackage = packageBuild(selectedBlueprint.typeID, selectedBlueprint.materialEfficiency, selectedBlueprint.timeEfficiency, runs, solarSystemID, mFacilityModifier, tFacilityModifier, tSkillModifier, tImplantModifiers, stationTax, .03)
        selectedBP = selectedBlueprint.itemID

    return render_template('/createPackage.html', ownedBlueprints=ownedBlueprints,  solarSystems=solarSystems, builtPackage=builtPackage, selectedBP=selectedBP, skills=skills,tImplantModifier=tImplantModifiers)


@app.route('/packages/shoppinglist')
def package_shoppinglist_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))
    packages = package.package.query.filter_by(charID=session['auth_user']['id'],stage=1).all()
    shoppingList = {}
    m3List = {}
    m3Total = 0
    costTotal = 0

    for item in packages:
        workingPackage = packageBuild(item.blueprintDataID, item.materialEfficiency, item.timeEfficiency, item.runs, item.sysID, item.facilityMEModifier, item.facilityTEModifier, 5, 5, item.tax,0)
        for material in workingPackage['materials']:
            type = esiGet.getTypeIDInfo(material['typeID'])
            if shoppingList.has_key(material['typeID']):
                shoppingList[material['typeID']] = shoppingList[material['typeID']] + material['quantity']
            else:
                shoppingList[material['typeID']] = material['quantity']
            if m3List.has_key(material['typeID']):
                m3List[material['typeID']] = m3List[material['typeID']] + (type.volume*material['quantity'])
            else:
                m3List[material['typeID']] = type.volume*material['quantity']
            m3Total = m3Total + type.volume*material['quantity']
            costTotal = costTotal + (getCurrentPricesFilter(material['typeID']) * material['quantity'])
    return render_template('shoppingList.html', shoppingList=shoppingList, m3Usage=m3List, m3Total=m3Total, costTotal=costTotal)


@app.route('/blueprints/list')
def blueprintlist_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))
    #refresh_XML_blueprints()
    refreshESIBlueprints()

    refreshTime = None

    defaultSolarSystemID = 30000190
    skills = {'industry': 5, 'advIndustry': 5}
    tImplantModifiers = 1
    tSkillModifier = (1 - (skills['industry'] * .04)) * (1 - (skills['advIndustry'] * .03))

    bpList = blueprints.blueprints.query.filter_by(charID=session['auth_user']['id']).all()

    #for bp in bpList:
    #    newPackage = packageBuild(bp.typeID, bp.materialEfficiency, bp.timeEfficiency, 1, defaultSolarSystemID , 1, 1, tSkillModifier, tImplantModifiers, .1)
    #    package[bp.itemID] = newPackage
    currentTime = datetime.datetime.utcnow()

    if bpList != []:
        refreshTime = bpList[0].cachedUntil
    else:
        refreshTime = currentTime
    cacheTimerRemaining = int((refreshTime - currentTime).total_seconds())

    return render_template('blueprintslist.html', bpList=bpList, cacheTimerRemaining=cacheTimerRemaining)


@app.route('/marketOrders/list')
def marketorders_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    refresh_XML_marketOrders()


    refreshTime = None

    marketList = marketOrders.marketOrders.query.filter(marketOrders.marketOrders.charID == session['auth_user']['id'], marketOrders.marketOrders.volRemaining > 0, marketOrders.marketOrders.bid == False).all()

    currentTime = datetime.datetime.utcnow()

    if marketList != []:
        refreshTime = marketList[0].cachedUntil
    else:
        refreshTime = currentTime

    cacheTimerRemaining = int((refreshTime - currentTime).total_seconds())

    return render_template('marketOrders.html',marketOrders=marketList,cacheTimerRemaining=cacheTimerRemaining)


@app.route('/industryJobs/list')
def industryJobs_page():
    session_status = check_and_refresh_session()
    if session_status == False:
        return redirect(url_for('main_page'))

    refresh_XML_industryJobs()

    refreshTime = None

    jobsList = industryJobs.industryJobs.query.filter_by(charID=session['auth_user']['id']).all()

    currentTime = datetime.datetime.utcnow()

    if jobsList != []:
        refreshTime = jobsList[0].cachedUntil
    else:
        refreshTime = currentTime
    cacheTimerRemaining = int((refreshTime - currentTime).total_seconds())

    return render_template('industryJobs.html', jobs=jobsList, cacheTimerRemaining=cacheTimerRemaining)


@app.route('/market')
def market_page():
    typeID = request.args.get('typeID')
    lastXDays = request.args.get('lastXDays')

    if typeID == None:
        typeID = 32772
    if lastXDays == None:
        lastXDays = 30

    marketHistory = {}
    marketHistory['The Forge'] = []
    marketHistory['Domain'] = []
    marketHistory['Heimatar'] = []
    marketHistory['Sinq Laison'] = []
    marketHistory['Metropolis'] = []

    regionMarkets = [10000002, 10000043, 10000030, 10000032, 10000042]

    marketOrders = {'buy':[],'sell':[]}


    for region in regionMarkets:
        esiGet.getMarketHistory(region, typeID)
        orders = esiGet.getRegionMarketOrders(region,typeID)
        for order in orders:
            if order[u'is_buy_order'] == False:
                marketOrders['sell'].append(order)
            else:
                marketOrders['buy'].append(order)

    startDate = (datetime.datetime.utcnow() + datetime.timedelta(-int(lastXDays)))

    # Set Random Colors for Regions
    colorSetting = {}
    for item in marketHistory:
        colorSetting[item] = genRandomColor()

    # Get Market History Data and Move into a Better Format for ChartJS & Jinja2
    marketHistoryQuery = marketHistoryData.marketHistoryData.query.filter(marketHistoryData.marketHistoryData.typeID==typeID,marketHistoryData.marketHistoryData.date >= startDate).order_by(marketHistoryData.marketHistoryData.date.asc())

    for item in marketHistoryQuery:
        if item.regionID == 10000002:
            marketHistory['The Forge'].append(item)
        elif item.regionID == 10000043:
            marketHistory['Domain'].append(item)
        elif item.regionID == 10000030:
            marketHistory['Heimatar'].append(item)
        elif item.regionID == 10000032:
            marketHistory['Sinq Laison'].append(item)
        elif item.regionID == 10000042:
            marketHistory['Metropolis'].append(item)

    # Generate X Axis Labeling
    dateSet = []
    for item in marketHistoryQuery:
        dateString = item.date.strftime("%m-%d-%y")
        if dateString not in dateSet:
            dateSet.append(dateString)

    # Generate Dropdown Selector List
    productList = []
    for bp in sdeData['blueprints']:
        if sdeData['blueprints'][bp]['activities'].has_key('manufacturing'):
            if sdeData['blueprints'][bp]['activities']['manufacturing'].has_key('products'):
                for product in sdeData['blueprints'][bp]['activities']['manufacturing']['products']:
                    productList.append(int(product['typeID']))

    return render_template('marketData.html', marketHistory=marketHistory, dateSet=dateSet, colorSetting=colorSetting, productList=productList, marketOrders=marketOrders, selectedTypeID=int(typeID), lastXDays=int(lastXDays))


#Main Functions
################################################################################################
def packageBuild(bpTypeID, me, te, runs, systemID, facilityMEModifier, facilityTEModifier, tSkillModifier, tImplantModifier, stationTax, salesTax):
    check_and_refresh_session()
    currentUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'], accessToken=session['auth_data']['access_token']).first()

    blueprintData = sdeData['blueprints'][int(bpTypeID)]['activities']

    defaultBuyLocation = currentUserQuery.defaultPriceLocationID
    defaultBuyLocationType = None
    if currentUserQuery.defaultPriceLocationType == 0:
        defaultBuyLocationType = 'region'
    else:
        defaultBuyLocationType = 'solarSystem'

    totalFees = criusIndustry.getAllJobFees(int(bpTypeID), int(systemID), "1", int(runs), float(stationTax))

    packageData = {}
    packageData['skillsNeeded'] = blueprintData['manufacturing']['skills']
    packageData['products'] = []
    packageData['materials'] = []
    packageData['bpTypeID'] = int(bpTypeID)
    packageData['me'] = int(me)
    packageData['te'] = int(te)
    packageData['runs'] = int(runs)
    packageData['systemID'] = int(systemID)
    packageData['systemIndex'] = criusIndustry.getSystemCostIndex(int(systemID),1)
    packageData['facilityMEModifier'] = float(facilityMEModifier)
    packageData['facilityTEModifier'] = float(facilityTEModifier)
    packageData['stationTax'] = float(stationTax)
    packageData['totalFees'] = totalFees
    packageData['buildTime'] = criusIndustry.getProductionTime(blueprintData['manufacturing']['time'], ((1-(float(te)*.01)) * tImplantModifier * tSkillModifier * float(facilityTEModifier)), int(runs))
    packageData['totalMaterialCost'] = 0.00
    packageData['totalSalePrice'] = 0.00
    packageData['expectedProfit'] = 0.00
    packageData['salesTax'] = float(salesTax)

    materialModifier = criusIndustry.getMaterialModifier(float(1-(float(me)/100)),float(facilityMEModifier))

    for item in blueprintData['manufacturing']['products']:

        packageData['products'].append({'typeID':item['typeID'],'quantity':item['quantity']*int(runs)})
        packageData['totalSalePrice'] = packageData['totalSalePrice'] + (esiGet.getMarketPrice(defaultBuyLocationType, defaultBuyLocation, item['typeID'],accessToken=session['auth_data']['access_token'])['sell']['low'] * packageData['products'][0]['quantity'])  # Rewrite to allow sell/buy low/high
    for item in blueprintData['manufacturing']['materials']:
        realMaterialRequirement = (criusIndustry.getProductionRequiredMaterials(int(runs),item['quantity'],materialModifier))

        materialPricePer = esiGet.getMarketPrice(defaultBuyLocationType,defaultBuyLocation,item['typeID'])[currentUserQuery.defaultPriceCalcType][currentUserQuery.defaultPriceCalcHighLow]
        requirementCost = realMaterialRequirement * materialPricePer
        packageData['totalMaterialCost'] = packageData['totalMaterialCost'] + requirementCost
        packageData['materials'].append({'typeID':item['typeID'],'quantity':int(realMaterialRequirement),'pricePer':float(materialPricePer),'requirementCost':float(requirementCost)})

    packageData['expectedProfit'] = packageData['totalSalePrice'] - (packageData['totalMaterialCost'] + packageData['totalFees'] + (packageData['totalFees'] * packageData['stationTax']) + (packageData['totalSalePrice'] * packageData['salesTax']))

    return packageData


def refresh_XML_accountBalance():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:

        cached = False
        accountBalanceQuery = accountBalance.accountBalance.query.filter_by(charID=session['auth_user']['id'], type=type).all()
        if accountBalanceQuery != None:
            for item in accountBalanceQuery:
                if item.is_valid() == False:
                    db.session.delete(item)
                else:
                    cached = True
            db.session.commit()
        if cached == False:
            accountBalanceXML = xmlAPIGet.retrieveXML('AccountBalance.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'])
            accountBalanceData = xmlAPIGet.rowsetparse(accountBalanceXML)
            cacheTimer = (datetime.datetime.strptime(xmlAPIGet.elementparse("cachedUntil", accountBalanceXML)[0:19], time_format))
            for item in accountBalanceData:
                newAccountBalance = accountBalance.accountBalance(session['auth_user']['id'], type, accountBalanceData[item]['accountID'], accountBalanceData[item]['accountKey'],
                                                                  accountBalanceData[item]['balance'], cacheTimer)
                db.session.add(newAccountBalance)
            db.session.commit()
    return


def refresh_XML_walletJournal():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:
        walletJournalXML = ""
        if typeArray == 'char':
            walletJournalXML = xmlAPIGet.retrieveXML('WalletTransactions.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'], rowCount=2560)
            walletJournalData = xmlAPIGet.rowsetparse(walletJournalXML)
            print walletJournalData
        elif typeArray == 'corp':
            journalIDArray = ['1000','1001','1002','1003','1004','1005','1006']
            for journalID in journalIDArray:
                walletJournalXML = xmlAPIGet.retrieveXML('WalletTransactions.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'],rowCount=2560, accountKey=journalID)
                walletJournalData = xmlAPIGet.rowsetparse(walletJournalXML)
                print walletJournalData

    return


def refresh_XML_assetList():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:

        cached = False
        assetListQuery = assetList.assetList.query.filter_by(charID=session['auth_user']['id'], type=type).all()
        if assetListQuery != None:
            for item in assetListQuery:
                if item.is_valid() == False:
                    db.session.delete(item)
                else:
                    cached = True
            db.session.commit()
        if cached == False:
            assetListXML = xmlAPIGet.retrieveXML('AssetList.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'], flat='1')
            assetListData = xmlAPIGet.rowsetparse(assetListXML)
            cacheTimer = (datetime.datetime.strptime(xmlAPIGet.elementparse("cachedUntil", assetListXML)[0:19], time_format))
            for item in assetListData:
                rawQuantity = None
                if assetListData[item].has_key('rawQuantity') == False:
                    rawQuantity = 0
                else:
                    rawQuantity = assetListData[item]['rawQuantity']
                newAssetList = assetList.assetList(session['auth_user']['id'], type, assetListData[item]['itemID'], assetListData[item]['locationID'], assetListData[item]['typeID'],
                                                   assetListData[item]['quantity'], assetListData[item]['flag'], assetListData[item]['singleton'], rawQuantity, cacheTimer)
                db.session.add(newAssetList)
            db.session.commit()
    return


def refresh_XML_blueprints():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:

        cached = False
        blueprintsQuery = blueprints.blueprints.query.filter_by(charID=session['auth_user']['id'], type=type).all()
        if blueprintsQuery != None:
            for item in blueprintsQuery:
                if item.is_valid() == False:
                    db.session.delete(item)
                else:
                    cached = True
            db.session.commit()
        if cached == False or blueprintsQuery == None:
            blueprintsListXML = xmlAPIGet.retrieveXML('Blueprints.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'])
            blueprintsListData = xmlAPIGet.rowsetparse(blueprintsListXML)
            cacheTimer = (datetime.datetime.strptime(xmlAPIGet.elementparse("cachedUntil", blueprintsListXML)[0:19], time_format))
            for item in blueprintsListData:
                newBlueprint = blueprints.blueprints(session['auth_user']['id'], type, blueprintsListData[item]['itemID'], blueprintsListData[item]['locationID'], blueprintsListData[item]['typeID'],
                                                     blueprintsListData[item]['quantity'], blueprintsListData[item]['flagID'], blueprintsListData[item]['timeEfficiency'],
                                                     blueprintsListData[item]['materialEfficiency'], blueprintsListData[item]['runs'], cacheTimer)
                db.session.add(newBlueprint)
            db.session.commit()
    return


def refreshESIBlueprints():
    time_format = "%Y-%m-%d %H:%M:%S"

    charID = session['auth_user']['id']
    accessToken = session['auth_data']['access_token']
    corpID = esiGet.getCharacterData(charID)["corporation_id"]

    cached = False
    blueprintsQuery = blueprints.blueprints.query.filter_by(charID=session['auth_user']['id']).all()
    if blueprintsQuery != None:
        for item in blueprintsQuery:
            if item.is_valid() == False:
                db.session.delete(item)
            else:
                cached = True
        db.session.commit()

    if cached == False or blueprintsQuery == None:

        characterBluprints = esiGet.getCharacterBlueprints(charID, accessToken)
        corpBlueprints = esiGet.getCorpBlueprints(corpID, accessToken)

        bpArray = [characterBluprints,corpBlueprints]

        typeArray = ['char', 'corp']
        for type in typeArray:
            workingArray = None
            if type == 'char':
                workingArray = characterBluprints
            else:
                workingArray = corpBlueprints
            if workingArray != None:
                cacheTimer = (datetime.datetime.utcnow() + datetime.timedelta(0,3600))
                for bp in workingArray:
                    newBlueprint = blueprints.blueprints(charID, type, bp["item_id"], bp["location_id"], bp["type_id"], bp["quantity"], 0, bp["time_efficiency"], bp['material_efficiency'], bp['runs'], cacheTimer)
                    db.session.add(newBlueprint)
                db.session.commit()
    return


def refresh_XML_industryJobs():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:

        cached = False
        industryJobsQuery = industryJobs.industryJobs.query.filter_by(charID=session['auth_user']['id'], type=type).all()
        if industryJobsQuery != None:
            for item in industryJobsQuery:
                if item.is_valid() == False:
                    db.session.delete(item)
                else:
                    cached = True
            db.session.commit()
        if cached == False:
            industryJobsXML = xmlAPIGet.retrieveXML('IndustryJobs.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'])
            industryJobsData = xmlAPIGet.rowsetparse(industryJobsXML)
            cacheTimer = (datetime.datetime.strptime(xmlAPIGet.elementparse("cachedUntil", industryJobsXML)[0:19], time_format))
            for item in industryJobsData:

                newIndustryJob = industryJobs.industryJobs(session['auth_user']['id'], type, industryJobsData[item]['jobID'], industryJobsData[item]['installerID'], industryJobsData[item]['facilityID'],
                                                           industryJobsData[item]['solarSystemID'], industryJobsData[item]['stationID'], industryJobsData[item]['activityID'],
                                                           industryJobsData[item]['blueprintID'], industryJobsData[item]['blueprintTypeID'], industryJobsData[item]['blueprintLocationID'],
                                                           industryJobsData[item]['outputLocationID'], industryJobsData[item]['runs'], industryJobsData[item]['cost'],
                                                           industryJobsData[item]['licensedRuns'], industryJobsData[item]['probability'], industryJobsData[item]['productTypeID'],
                                                           industryJobsData[item]['status'], industryJobsData[item]['timeInSeconds'], datetime.datetime.strptime(industryJobsData[item]['startDate'][0:19],time_format),
                                                           datetime.datetime.strptime(industryJobsData[item]['endDate'][0:19], time_format), datetime.datetime.strptime(industryJobsData[item]['pauseDate'][0:19],time_format),
                                                           datetime.datetime.strptime(industryJobsData[item]['completedDate'][0:19], time_format), industryJobsData[item]['completedCharacterID'], industryJobsData[item]['successfulRuns'], cacheTimer)
                db.session.add(newIndustryJob)
            db.session.commit()
    return


def refresh_XML_marketOrders():
    time_format = "%Y-%m-%d %H:%M:%S"

    typeArray = ['char', 'corp']

    for type in typeArray:

        cached = False
        marketOrdersQuery = marketOrders.marketOrders.query.filter_by(charID=session['auth_user']['id'], type=type).all()
        if marketOrdersQuery != None:
            for item in marketOrdersQuery:
                if item.is_valid() == False:
                    db.session.delete(item)
                else:
                    cached = True

            db.session.commit()
        if cached == False:
            marketOrdersXML = xmlAPIGet.retrieveXML('MarketOrders.xml.aspx', type, session['auth_data']['access_token'], characterID=session['auth_user']['id'])
            marketOrdersData = xmlAPIGet.rowsetparse(marketOrdersXML)
            cacheTimer = (datetime.datetime.strptime(xmlAPIGet.elementparse("cachedUntil", marketOrdersXML)[0:19], time_format))
            for item in marketOrdersData:
                issuedDateTime = datetime.datetime.strptime(marketOrdersData[item]['issued'][0:19], time_format)
                newMarketOrder = marketOrders.marketOrders(session['auth_user']['id'], type, marketOrdersData[item]['orderID'], marketOrdersData[item]['charID'], marketOrdersData[item]['stationID'],
                                                           marketOrdersData[item]['volEntered'], marketOrdersData[item]['volRemaining'], marketOrdersData[item]['minVolume'],
                                                           marketOrdersData[item]['orderState'], marketOrdersData[item]['typeID'], marketOrdersData[item]['range'], marketOrdersData[item]['accountKey'],
                                                           marketOrdersData[item]['duration'], marketOrdersData[item]['escrow'], marketOrdersData[item]['price'], marketOrdersData[item]['bid'],
                                                           issuedDateTime, cacheTimer)
                db.session.add(newMarketOrder)
            db.session.commit()
    return


def check_and_refresh_session():
    if session.has_key('auth_user'):
        existingUserQuery = pilot.pilot.query.filter_by(charID=session['auth_user']['id'], accessToken=session['auth_data']['access_token']).first()
        if existingUserQuery != None:
            if existingUserQuery.is_valid() == False:
                accessTokenData = crestAuth.get_access_token(client_id, secret_key, existingUserQuery.refreshToken, 'refresh')
                if accessTokenData.has_key("access_token"):
                    existingUserQuery.updateToken(accessTokenData['access_token'], accessTokenData['refresh_token'], accessTokenData['expires_in'])
                    session['auth_data'] = accessTokenData
                    return True
                else:
                    print accessTokenData + " : " + existingUserQuery.refreshToken
                    session.clear()
                    return False
        else:
            session.clear()
            return False
    else:
        session.clear()
        return False


if __name__ == "app":
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=debugMode, host='0.0.0.0',port=runPort)
