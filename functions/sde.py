import os
import yaml
import csv

from classes import mapSolarSystems
from classes import mapRegions
from classes import staStations

import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

def readSDECSV(fileLoc):
    csvData =  {}
    entry = 1
    with open(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/data/sde/' + fileLoc, "r") as fileHandle:
        File = csv.reader(fileHandle)
        headers = File.next()
        for row in File:
            csvData[int(entry)]={}
            inc = 0
            for h in headers:
                if row[inc] == "None":
                    csvData[int(entry)][h] = 0
                else:
                    csvData[int(entry)][h] = row[inc]
                inc = inc + 1
            entry = entry + 1

    print('Finished Processing File: ' + fileLoc + ' - ' + str(entry-1) + " lines")
    return csvData

def readSDE():
    print("Reading SDE Data")
    sde = {}
    for filename in os.listdir(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) +'/data/sde/fsd'):
        if filename.endswith('.yaml') and filename != 'typeIDs.yaml':
            f = open(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) +'/data/sde/fsd/' + filename)
            dataMap = yaml.safe_load(f)
            f.close()
            sde[filename[:-5]] = dataMap
    return sde

def readMapCSV():

    oldMapRegionData = mapRegions.mapRegions.query.all()
    oldSolarSystemData = mapSolarSystems.mapSolarSystems.query.all()
    oldStationData = staStations.staStations.query.all()

    for entry in oldSolarSystemData:
        db.session.delete(entry)
    for entry in oldMapRegionData:
        db.session.delete(entry)
    for entry in oldStationData:
        db.session.delete(entry)
    db.session.commit()

    mapRegionData = readSDECSV("mapRegions.csv")
    for item in mapRegionData:
        newItem = mapRegions.mapRegions(mapRegionData[item]['regionID'], mapRegionData[item]['regionName'], mapRegionData[item]['x'], mapRegionData[item]['y'], mapRegionData[item]['z'],
                                        mapRegionData[item]['xMin'], mapRegionData[item]['xMax'], mapRegionData[item]['yMin'], mapRegionData[item]['yMax'], mapRegionData[item]['zMin'],
                                        mapRegionData[item]['zMax'], mapRegionData[item]['factionID'], mapRegionData[item]['radius'])
        db.session.add(newItem)
    db.session.commit()

    mapSolarSystemData = readSDECSV("mapSolarSystems.csv")
    for item in mapSolarSystemData:
        newItem = mapSolarSystems.mapSolarSystems(mapSolarSystemData[item]['regionID'], mapSolarSystemData[item]['constellationID'], mapSolarSystemData[item]['solarSystemID'],
                                                  mapSolarSystemData[item]['solarSystemName'], mapSolarSystemData[item]['x'], mapSolarSystemData[item]['y'], mapSolarSystemData[item]['z'],
                                                  mapSolarSystemData[item]['xMin'], mapSolarSystemData[item]['xMax'], mapSolarSystemData[item]['yMin'], mapSolarSystemData[item]['yMax'],
                                                  mapSolarSystemData[item]['zMin'], mapSolarSystemData[item]['zMax'], mapSolarSystemData[item]['luminosity'], mapSolarSystemData[item]['border'],
                                                  mapSolarSystemData[item]['fringe'], mapSolarSystemData[item]['corridor'], mapSolarSystemData[item]['hub'], mapSolarSystemData[item]['international'],
                                                  mapSolarSystemData[item]['regional'], mapSolarSystemData[item]['constellation'], mapSolarSystemData[item]['security'],
                                                  mapSolarSystemData[item]['factionID'], mapSolarSystemData[item]['radius'], mapSolarSystemData[item]['sunTypeID'],
                                                  mapSolarSystemData[item]['securityClass'])
        db.session.add(newItem)
    db.session.commit()

    staStationData = readSDECSV("staStations.csv")
    for item in staStationData:
        newItem = staStations.staStations(staStationData[item]['stationID'], staStationData[item]['security'], staStationData[item]['dockingCostPerVolume'],
                                          staStationData[item]['maxShipVolumeDockable'], staStationData[item]['officeRentalCost'], staStationData[item]['operationID'],
                                          staStationData[item]['stationTypeID'], staStationData[item]['corporationID'], staStationData[item]['solarSystemID'], staStationData[item]['constellationID'],
                                          staStationData[item]['regionID'], staStationData[item]['stationName'], staStationData[item]['x'], staStationData[item]['y'], staStationData[item]['z'],
                                          staStationData[item]['reprocessingEfficiency'], staStationData[item]['reprocessingStationsTake'], staStationData[item]['reprocessingHangarFlag'])
        db.session.add(newItem)
    db.session.commit()

    return