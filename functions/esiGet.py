import requests
import json
import datetime
import grequests
import time

from requests.exceptions import HTTPError

from classes import invTypes
from classes import mapSolarSystems,staStations
from classes import knownCitadel
from classes import priceCache
from classes import marketHistoryData
from classes import badTypeIDs
from classes import blueprints

import checkCitadels

import os

basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

# Functions
def concurrent_page_requests(pages, URL):
    reqs = []
    start_time = time.time()

    for page in range(2, pages + 1):
        req = grequests.get(URL, params={'page': page})
        reqs.append(req)

    responses = grequests.map(reqs)

    end_time = time.time()
    elapsed = end_time - start_time
    print('Elapsed time to ' + URL + ' : ' + str(len(responses)) + " / " + str(elapsed))

    return responses


# SDE DATA
######################################################################################
def getTypeIDInfo(typeID):
    badTypeIDQuery = badTypeIDs.badTypeIDs.query.filter_by(typeID=typeID).first()
    if badTypeIDQuery == None:
        typeIDQuery = invTypes.invTypes.query.filter_by(id=int(typeID)).first()
        typeIDData = None
        if typeIDQuery == None:

            url='https://esi.tech.ccp.is/latest/universe/types/' + str(typeID) + '/?datasource=tranquility&language=en-us&user_agent=EME3-esi'
            payload = ""
            headers = {'Accept:': 'application/json'}

            #r = requests.get(url, data=payload, headers=headers)
            r = requests.get(url, data=payload)
            typeIDData = json.loads(r.text)
            if typeIDData == {u'error': u'Type not found'}:
                newBadTypeID = badTypeIDs.badTypeIDs(typeID)
                db.session.add(newBadTypeID)
                db.session.commit()

            else:
                newTypeID =  invTypes.invTypes(typeID, typeIDData['name'], typeIDData['mass'], typeIDData['volume'], typeIDData['portion_size'])
                db.session.add(newTypeID)
                db.session.commit()
            typeIDData = invTypes.invTypes.query.filter_by(id=int(typeID)).first()
        else:
            typeIDData = typeIDQuery
        return typeIDData
    return

def getSystemInfo(systemID):
    url = 'https://esi.tech.ccp.is/latest/universe/systems/' + systemID +'/&user_agent=EME3-esi'

    payload = ""
    headers = {'Accept': 'application/json'}
    # r = requests.get(url, headers=headers)
    r = requests.get(url, data=payload)
    systemInfo = json.loads(r.text)

    return systemInfo

def getConstellationInfo(constellationID):
    url = 'https://esi.tech.ccp.is/latest/universe/constellations/' + constellationID +'/&user_agent=EME3-esi'

    payload = ""
    headers = {'Accept': 'application/json'}
    # r = requests.get(url, headers=headers)
    r = requests.get(url, data=payload)
    constellationInfo = json.loads(r.text)

    return constellationInfo

def getRegionInfo(regionID):

    url = 'https://esi.tech.ccp.is/latest/universe/regionss/' + regionID +'/&user_agent=EME3-esi'

    payload = ""
    headers = {'Accept': 'application/json'}
    # r = requests.get(url, headers=headers)
    r = requests.get(url, data=payload)
    regionInfo = json.loads(r.text)

    return regionInfo

# MARKET INFORMATION
######################################################################################
def getRegionMarketOrders(region,typeID,orderType='all'):


    url = 'https://esi.tech.ccp.is/latest/markets/' + str(region) + '/orders/?datasource=tranquility&order_type=' + orderType + '&type_id=' + str(typeID) + '&user_agent=EME3-esi'

    payload = ""
    headers = {'Accept': 'application/json'}
    #r = requests.get(url, headers=headers)
    r = requests.get(url, data=payload)
    marketOrdersData = json.loads(r.text)

    return marketOrdersData

def getSolarSystemMarketOrders(solarSystem,typeID,orderType='all',accessToken=None):
    solarSystemOrders = []
    region = mapSolarSystems.mapSolarSystems.query.filter_by(solarSystemID=int(solarSystem)).first().regionID
    #regionResults = getRegionMarketOrders(region,typeID,orderType=orderType)
    regionResults = getAllRegionMarketData(region, typeID=typeID, orderType=orderType)
    for order in regionResults:
        stationQuery = staStations.staStations.query.filter_by(stationID=int(order['location_id'])).first()
        if stationQuery != None:
            if stationQuery.solarSystemID == solarSystem:
                solarSystemOrders.append(order)
        else:
            citadelQuery = knownCitadel.knownCitadel.query.filter_by(citadelID=int(order['location_id'])).first()

            if citadelQuery != None:
                if citadelQuery.solarSystemID == solarSystem:
                    solarSystemOrders.append(order)
                citadelQuery.lastSeen = datetime.datetime.utcnow()
                db.session.commit()
            else:
                citadelData = checkCitadels.get_Citadel(int(order['location_id']),accessToken)

                if citadelData != None:

                    if citadelData.solarSystemID == solarSystem:
                        solarSystemOrders.append(order)


    return solarSystemOrders

def getMarketPrice(type,locationID,typeID,accessToken=None):
    lowBuyPrice = 9999999999999999.99
    highBuyPrice = 0
    lowSellPrice = 9999999999999999.99
    highSellPrice = 0
    priceCacheQuery = priceCache.priceCache.query.filter_by(typeID=int(typeID),type=type,locationID=locationID).first()
    if priceCacheQuery != None:
        if priceCacheQuery.is_valid():
            return {'buy': {'low': priceCacheQuery.buyLow, 'high': priceCacheQuery.buyHigh}, 'sell': {'low': priceCacheQuery.sellLow, 'high': priceCacheQuery.sellHigh}}
        else:
            db.session.delete(priceCacheQuery)
            db.session.commit()

    orders = []
    if type == "region":
        #orders = getRegionMarketOrders(locationID,typeID)
        orders = getAllRegionMarketData(locationID,typeID=typeID)
    elif type == 'solarSystem':
        orders = getSolarSystemMarketOrders(locationID,typeID,accessToken=accessToken)
    for order in orders:
        if order['is_buy_order'] == False:
            if order['price'] > highSellPrice:
                highSellPrice = order['price']
            elif order['price'] < lowSellPrice:
                lowSellPrice = order['price']
        elif order['is_buy_order'] == True:
            if order['price'] > highBuyPrice:
                highBuyPrice = order['price']
            elif order['price'] < lowBuyPrice:
                lowBuyPrice = order['price']

    newPriceCache = priceCache.priceCache(typeID,type,locationID,lowBuyPrice,highBuyPrice,lowSellPrice,highSellPrice)
    db.session.add(newPriceCache)
    db.session.commit()
    return {'buy':{'low':lowBuyPrice,'high':highBuyPrice},'sell':{'low':lowSellPrice,'high':highSellPrice}}

def getMarketHistory(region,typeID):

    url= "https://esi.tech.ccp.is/latest/markets/" + str(region) + "/history/?datasource=tranquility&type_id=" + str(typeID) + "&user_agent=EME3-esi"

    data = ((requests.get(url)).json())

    priceHistoryList = []
    for day in data:
        dayHistory = marketHistoryData.marketHistoryData.query.filter_by(typeID=typeID,regionID=region,date=datetime.datetime.strptime(day['date'],'%Y-%m-%d')).first()
        if dayHistory != None:
            priceData = {u'orderCount':dayHistory.orderCount,u'volume':dayHistory.volume,u'lowPrice':dayHistory.lowPrice,u'avgPrice':dayHistory.avgPrice,u'highPrice':dayHistory.highPrice,u'date':dayHistory.date}
        else:
            dayHistory = marketHistoryData.marketHistoryData(typeID, region, datetime.datetime.strptime(day['date'],'%Y-%m-%d'), day['highest'], day['average'], day['lowest'], day['volume'], day['order_count'])
            db.session.add(dayHistory)
            priceData = {u'orderCount': dayHistory.orderCount, u'volume': dayHistory.volume,u'lowPrice': dayHistory.lowPrice, u'avgPrice': dayHistory.avgPrice,u'highPrice': dayHistory.highPrice, u'date': dayHistory.date}
        priceHistoryList.append(priceData)
    db.session.commit()

    return priceHistoryList


def getAllRegionMarketData(regionID, typeID=None, orderType='all'):
    # Make Parallel Requests for all Pages

    MARKET_URL = "https://esi.tech.ccp.is/latest/markets/" + str(regionID) + "/orders/?order_type=" + orderType + '&user_agent=EME3-esi'

    if typeID != None:
        MARKET_URL = "https://esi.tech.ccp.is/latest/markets/" + str(regionID) + "/orders/?type_id=" + str(typeID) + "&order_type=" + orderType + '&user_agent=EME3-esi'

    all_orders = []
    req = grequests.get(MARKET_URL).send()
    res = req.response

    res.raise_for_status()

    all_orders.extend(res.json())
    pages = int(res.headers['X-Pages'])
    responses = concurrent_page_requests(pages, MARKET_URL)

    for response in responses:
        try:
            response.raise_for_status()
        except HTTPError:
            print('Received status code {} from {}'.format(response.status_code, response.url))
            continue

        data = response.json()
        all_orders.extend(data)
    return all_orders

# STRUCTURE AND STATION DATA
######################################################################################
def getStationData(location_ID):
    url = "https://esi.tech.ccp.is/latest/universe/stations/" + str(location_ID) + "/?datasource=tranquility&user_agent=EME3-esi"
    payload = ""
    headers = {'Accept:': 'application/json'}

    #r = requests.get(url, data=payload, headers=headers)
    r = requests.get(url, data=payload)

    stationData = json.loads(r.text)

    returnHeader = r.headers

    return stationData

def getStructuresData(location_ID,accessToken):
    url = "https://esi.tech.ccp.is/latest/universe/structures/" + str(location_ID) + "/?datasource=tranquility&token=" + str(accessToken) + "&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    #r = requests.get(url, data=payload, headers=headers)

    # Fix for Requests Bug
    r = requests.get(url, data=payload)

    structureData = json.loads(r.text)

    returnHeader = r.headers

    return structureData

#CHARACTER AND CORP DATA
######################################################################################
def getCharacterData(characterID):
    url = "https://esi.tech.ccp.is/latest/characters/" + str(characterID) + "/?datasource=tranquility&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    #r = requests.get(url, data=payload, headers=headers)

    r = requests.get(url, data=payload)

    characterData = json.loads(r.text)

    returnHeader = r.headers

    return characterData

def getCharacterBlueprints(characterID,accessToken):
    url = "https://esi.tech.ccp.is/latest/characters/" + str(characterID) + "/blueprints/?datasource=tranquility&token=" + str(accessToken) + "&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    # r = requests.get(url, data=payload, headers=headers)

    # Fix for Requests Bug
    r = requests.get(url, data=payload)

    characterBlueprintData = json.loads(r.text)

    returnHeader = r.headers

    return characterBlueprintData

def getCorpBlueprints(corpID,accessToken):
    url = "https://esi.tech.ccp.is/latest/corporations/" + str(corpID) + "/blueprints/?datasource=tranquility&token=" + str(accessToken) + "&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    # r = requests.get(url, data=payload, headers=headers)

    # Fix for Requests Bug
    r = requests.get(url, data=payload)

    corpBlueprintData = json.loads(r.text)

    returnHeader = r.headers

    return corpBlueprintData

def getCharacterAssets(characterID,accessToken):
    url = "https://esi.tech.ccp.is/latest/characters/" + str(characterID) + "/assets/?datasource=tranquility&token=" + str(accessToken) + "&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    # r = requests.get(url, data=payload, headers=headers)

    # Fix for Requests Bug
    r = requests.get(url, data=payload)

    characterAssetData = json.loads(r.text)

    returnHeader = r.headers

    return characterAssetData

def getCorpAssets(corpID,accessToken):
    url = "https://esi.tech.ccp.is/latest/corporations/" + str(corpID) + "/assets/?datasource=tranquility&token=" + str(accessToken) + "&user_agent=EME3-esi"
    payload = ""

    headers = {'Accept:': 'application/json'}
    # r = requests.get(url, data=payload, headers=headers)

    # Fix for Requests Bug
    r = requests.get(url, data=payload)

    corpAssetData = json.loads(r.text)

    returnHeader = r.headers

    return corpAssetData