from xml.etree import ElementTree as ET
import requests

####################################################################
#XML Parsing Functions
#rowsetparse: Returns all data in rowset style XML entries.
#             Attributes:XMLdata = Retrieved XML dictionary from
#             retrieveXML function
####################################################################
def rowsetparse(XMLdata):
    element = ET.XML(XMLdata)
    parseddata = {}
    rownum=0
    for subelement in element:
        for rowset in subelement:
            for row in rowset:
                parseddata[rownum]=row.attrib
                subrownum=0
                for subrowset in row:
                    for subrow in subrowset:
                       parseddata[row][subrownum]=subrow.attrib
                       subrownum=subrownum+1
                rownum=rownum+1
    return parseddata

def elementparse(elementtag,data):
    elementdata = ET.XML(data)

    for element in elementdata:
        if element.tag == elementtag:
            returnedElementData = element.text
            return returnedElementData

def retrieveXML(endpoint,accessType,accessToken,**kwargs):

    url = 'https://api.eveonline.com/' + accessType +"/" + endpoint + "?accessToken=" + accessToken
    for name,value in kwargs.items():
        url = str(url) + "&" + str(name) + "=" + str(value)

    headers = {'User-Agent': 'EME3', 'content-type': 'application/x-www-form-urlencoded'}
    r = requests.get(url,  headers=headers)

    data = r.text

    return data
