import uuid
import requests
import base64
import json

def get_auth_URL(client_id,state):

    response_type="code"
    redirect_uri="http://eme3.divby0.net:52382/crestAuth/"

    scope="publicData characterStatsRead characterLocationRead characterWalletRead characterAssetsRead characterIndustryJobsRead characterMarketOrdersRead characterAccountRead characterContractsRead corporationWalletRead corporationAssetsRead corporationIndustryJobsRead corporationMarketOrdersRead corporationContractsRead esi-search.search_structures.v1 esi-universe.read_structures.v1 esi-markets.structure_markets.v1 esi-corporations.read_structures.v1 esi-corporations.read_blueprints.v1 esi-characters.read_blueprints.v1"
    crestAPIURI="https://login.eveonline.com/oauth/authorize/"

    redirectURL = crestAPIURI +"?response_type="+response_type+"&redirect_uri="+redirect_uri+"&client_id="+client_id+"&scope="+scope+"&state="+state

    return redirectURL

def get_access_token(client_id,secret_key,code,type):
    clientSecret = client_id + ":" + secret_key
    url = ""
    if type == "refresh":
        url = 'https://login.eveonline.com/oauth/token?grant_type=refresh_token&refresh_token=' + code
    elif type == "auth":
        url = 'https://login.eveonline.com/oauth/token?grant_type=authorization_code&code=' + code

    payload = ""
    headers = {'Authorization': 'Basic ' + base64.b64encode(clientSecret), 'content-type': 'application/x-www-form-urlencoded'}

    r = requests.post(url, data=payload, headers=headers)

    crest_auth_return = json.loads(r.text)

    return crest_auth_return

def get_auth_character(access_token):
    url = 'https://crest-tq.eveonline.com/decode/'

    payload = ""
    headers = {'Authorization': 'Bearer ' + access_token, 'content-type': 'application/json'}
    r = requests.get(url, data=payload,headers=headers)

    crest_charlist_return = json.loads(r.text)

    url = crest_charlist_return['character']['href']
    r = requests.get(url,data=payload,headers=headers)

    crest_char_return = json.loads(r.text)

    return crest_char_return