import math
import requests

from xml.dom import minidom

###############################################################################
# Production Functions
##############################################################################

# Calculates the Material Modifier for a Production Run
def getMaterialModifier(ME_Modifier,facilityModifier):
    materialModifier = (ME_Modifier * facilityModifier)
    return materialModifier

# Calculates Required Materials to Manufacture
def getProductionRequiredMaterials(runs,baseQuantity,materialModifier):
    required = max(runs,math.ceil(round(runs*baseQuantity*materialModifier,2)))
    return required

# Calculates Required Production Time
def getProductionTime(baseProductionTime,timeModifier,runs):
    prodTime = baseProductionTime*timeModifier*runs
    return prodTime

################################################################################
# Job Cost Functions
################################################################################

# Calculates Job Installation Fee(Job Fee)
def getJobFee(baseJobCost,systemCostIndex,runs):
    installationFee = baseJobCost*systemCostIndex*runs
    return installationFee

#Calculates Job Base Cost based on typeID
def getJobBaseCost(typeID):
    eveIndustryURL = "http://api.eve-industry.org/job-base-cost.xml?ids=" + str(typeID)
    #jobBaseCostXML = urllib2.urlopen(eveIndustryURL)
    jobBaseCostXML = requests.get(eveIndustryURL).text

    xmldoc = minidom.parseString(jobBaseCostXML)
    jobBaseCostList = xmldoc.getElementsByTagName('job-base-cost')
    for node in jobBaseCostList:
        return node.childNodes[0].nodeValue

# Retrieves the System Cost Index from the Eve Industry API per Job Type
def getSystemCostIndex(systemID,activityID):
    eveIndustryURL = "http://api.eve-industry.org/system-cost-index.xml?id=" + str(systemID)
    #systemCostIndexXML = urllib2.urlopen(eveIndustryURL)
    systemCostIndexXML = requests.get(eveIndustryURL).text
    xmldoc = minidom.parseString(systemCostIndexXML)
    systemCostIndexList = xmldoc.getElementsByTagName('activity')
    for node in systemCostIndexList:
        if node.attributes["id"].nodeValue == str(activityID):
            return node.childNodes[0].nodeValue

# Retrieves the Facilities Tax
def getFacilityTax(jobFee,taxRate):
    facilityTax = (jobFee) * (taxRate)
    #print ("Debug: Facility Tax:" + str(facilityTax))
    return facilityTax

# Retrieves the Total Installation Cost
def getTotalInstallationCost(jobFee,facilityTax):
    totalInstallationCost = jobFee + facilityTax
    return totalInstallationCost

# Determines the Final Job Fees associated with a Job
def getAllJobFees(typeID,systemID,jobType,runs,tax,systemCostIndex = "None", baseCost = "None"):
    if baseCost == "None":
        buildBaseCost = float(getJobBaseCost(typeID))
    else:
        buildBaseCost = float(baseCost)
    if systemCostIndex == "None":
        buildSystemCostIndex = getSystemCostIndex(systemID,jobType)
    else:
        buildSystemCostIndex = round(systemCostIndex,2)
    buildJobFee = getJobFee(float(buildBaseCost),float(buildSystemCostIndex),int(runs))

    buildFacilityTax = getFacilityTax(buildJobFee,tax)
    buildTotalInstallationCost = round(getTotalInstallationCost(buildJobFee,buildFacilityTax),2)
    return buildTotalInstallationCost
