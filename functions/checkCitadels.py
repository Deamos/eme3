import requests
import json
from classes import knownCitadel
from classes import mapSolarSystems
import esiGet

import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

def get_Citadel(stationID,accessToken):
    citadelQuery = knownCitadel.knownCitadel.query.filter_by(citadelID=stationID).first()
    if citadelQuery == None:
        structureData = esiGet.getStructuresData(stationID,accessToken)
        if structureData.has_key("error") == False:
            solarSystemQuery = mapSolarSystems.mapSolarSystems.query.filter_by(solarSystemID=int(structureData["solar_system_id"])).first()

            newKnownCitadel = knownCitadel.knownCitadel(stationID, structureData["solar_system_id"], solarSystemQuery.regionID,
                                                        structureData['solar_system_id'], structureData['name'])
            db.session.add(newKnownCitadel)
            db.session.commit()
            return newKnownCitadel
        if structureData.has_key("error"):
            print structureData
    else:
        return citadelQuery
    return None