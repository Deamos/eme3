import requests
import json
import datetime
from classes import invTypes
from classes import mapSolarSystems,staStations
from classes import knownCitadel
from classes import priceCache
from classes import marketHistoryData
from classes import badTypeIDs

import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

import checkCitadels

def getTypeIDInfo(typeID):
    badTypeIDQuery = badTypeIDs.badTypeIDs.query.filter_by(typeID=typeID).first()
    if badTypeIDQuery == None:
        typeIDQuery = invTypes.invTypes.query.filter_by(id=int(typeID)).first()
        typeIDData = None
        if typeIDQuery == None:

            url='https://crest-tq.eveonline.com/inventory/types/' + str(typeID) + '/'
            payload = ""
            headers = {'User-Agent':'EME3', 'content-type': 'application/x-www-form-urlencoded'}

            r = requests.get(url, data=payload, headers=headers)
            typeIDData = json.loads(r.text)
            if typeIDData == {u'message': u'Type not found', u'exceptionType': u'NotFoundError', u'key': u'typeNotFound'}:
                newBadTypeID = badTypeIDs.badTypeIDs(typeID)
                db.session.add(newBadTypeID)
                db.session.commit()

            else:
                newTypeID =  invTypes.invTypes(typeID, typeIDData['name'], typeIDData['mass'], typeIDData['volume'], typeIDData['portionSize'])
                db.session.add(newTypeID)
                db.session.commit()
            typeIDData = invTypes.invTypes.query.filter_by(id=int(typeID)).first()
        else:
            typeIDData = typeIDQuery
        return typeIDData
    return


def getRegionMarketOrders(region,typeID,buy=None):
    url='https://crest-tq.eveonline.com/market/' + str(region) + '/orders/?type=https://crest-tq.eveonline.com/inventory/types/' + str(typeID) +'/'
    payload = ""
    headers = {'User-Agent': 'EME3', 'content-type': 'application/json'}
    r = requests.get(url, headers=headers)
    marketOrders = json.loads(r.text)
    return marketOrders['items']

def getSolarSystemMarketOrders(solarSystem,typeID,buy=None):
    solarSystemOrders = []
    region = mapSolarSystems.mapSolarSystems.query.filter_by(solarSystemID=int(solarSystem)).first().regionID
    regionResults = getRegionMarketOrders(region,typeID)
    for order in regionResults:
        stationQuery = staStations.staStations.query.filter_by(stationID=int(order['location']['id'])).first()
        if stationQuery != None:
            if stationQuery.solarSystemID == solarSystem:
                solarSystemOrders.append(order)
        else:
            citadelQuery = knownCitadel.knownCitadel.query.filter_by(citadelID=order['location']['id']).first()

            if citadelQuery != None:
                if citadelQuery.solarSystemID == solarSystem:
                    solarSystemOrders.append(order)
                citadelQuery.lastSeen = datetime.datetime.utcnow()
                db.session.commit()
            else:
                citadelData = checkCitadels.get_Citadel(order['location']['id'])
                #url = 'https://stop.hammerti.me.uk/api/citadel/' + str(order['location']['id'])
                #payload = ""
                #headers = {'User-Agent': 'EME3', 'content-type': 'application/json'}
                #r = requests.get(url, headers=headers)
                #citadelData = json.loads(r.text)
                if citadelData != None:
                    citadelid = str(order['location']['id'])
                    if citadelData[citadelid]['systemId'] == solarSystem:
                        solarSystemOrders.append(order)
                    #newKnownCitadel = knownCitadel.knownCitadel(order['location']['id'],citadelData[str(order['location']['id'])]['typeId'],citadelData[str(order['location']['id'])]['regionId'],citadelData[str(order['location']['id'])]['systemId'],citadelData[str(order['location']['id'])]['name'])
                    #db.session.add(newKnownCitadel)
                    #db.session.commit()


    return solarSystemOrders

def getMarketPrice(type,locationID,typeID):
    lowBuyPrice = 9999999999999999.99
    highBuyPrice = 0
    lowSellPrice = 9999999999999999.99
    highSellPrice = 0
    priceCacheQuery = priceCache.priceCache.query.filter_by(typeID=int(typeID),type=type,locationID=locationID).first()
    if priceCacheQuery != None:
        if priceCacheQuery.is_valid():
            return {'buy': {'low': priceCacheQuery.buyLow, 'high': priceCacheQuery.buyHigh}, 'sell': {'low': priceCacheQuery.sellLow, 'high': priceCacheQuery.sellHigh}}
        else:
            db.session.delete(priceCacheQuery)
            db.session.commit()

    orders = []
    if type == "region":
        orders = getRegionMarketOrders(locationID,typeID)
    elif type == 'solarSystem':
        orders = getSolarSystemMarketOrders(locationID,typeID)
    for order in orders:
        if order['buy'] == False:
            if order['price'] > highSellPrice:
                highSellPrice = order['price']
            elif order['price'] < lowSellPrice:
                lowSellPrice = order['price']
        elif order['buy'] == True:
            if order['price'] > highBuyPrice:
                highBuyPrice = order['price']
            elif order['price'] < lowBuyPrice:
                lowBuyPrice = order['price']

    newPriceCache = priceCache.priceCache(typeID,type,locationID,lowBuyPrice,highBuyPrice,lowSellPrice,highSellPrice)
    db.session.add(newPriceCache)
    db.session.commit()
    return {'buy':{'low':lowBuyPrice,'high':highBuyPrice},'sell':{'low':lowSellPrice,'high':highSellPrice}}

def getMarketHistory(region,typeID):

    #"https://crest-tq.eveonline.com/market/" + str(region) + "/history/?type=https://crest-tq.eveonline.com/types/" + str(typeID) + "/"
    url = "https://crest-tq.eveonline.com/market/" + str(region) + "/history/?type=https://crest-tq.eveonline.com/inventory/types/" + str(typeID) + "/"

    data = ((requests.get(url)).json())[u'items']

    priceHistoryList = []
    for day in data:

        dayHistory = marketHistoryData.marketHistoryData.query.filter_by(typeID=typeID,regionID=region,date=datetime.datetime.strptime(day['date'][:-9],'%Y-%m-%d')).first()
        if dayHistory != None:
            priceData = {u'orderCount':dayHistory.orderCount,u'volume':dayHistory.volume,u'lowPrice':dayHistory.lowPrice,u'avgPrice':dayHistory.avgPrice,u'highPrice':dayHistory.highPrice,u'date':dayHistory.date}
        else:
            dayHistory = marketHistoryData.marketHistoryData(typeID, region, datetime.datetime.strptime(day['date'][:-9],'%Y-%m-%d'), day['highPrice'], day['avgPrice'], day['lowPrice'], day['volume'], day['orderCount'])
            db.session.add(dayHistory)
            priceData = {u'orderCount': dayHistory.orderCount, u'volume': dayHistory.volume,u'lowPrice': dayHistory.lowPrice, u'avgPrice': dayHistory.avgPrice,u'highPrice': dayHistory.highPrice, u'date': dayHistory.date}
        priceHistoryList.append(priceData)
    db.session.commit()

    return priceHistoryList
