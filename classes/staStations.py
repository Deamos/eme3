import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class staStations(db.Model):
    __tablename__ = "staStations"
    id = db.Column(db.Integer, primary_key=True)
    stationID = db.Column(db.Integer, unique=True)
    security = db.Column(db.Integer)
    dockingCostPerVolume = db.Column(db.Float)
    maxShipVolumeDockable = db.Column(db.Integer)
    officeRentalCost = db.Column(db.Integer)
    operationID = db.Column(db.Integer)
    stationTypeID = db.Column(db.Integer)
    corporationID = db.Column(db.Integer)
    solarSystemID = db.Column(db.Integer)
    constellationID = db.Column(db.Integer)
    regionID = db.Column(db.Integer)
    stationName = db.Column(db.String(256))
    x = db.Column(db.Float)
    y = db.Column(db.Float)
    z = db.Column(db.Float)
    reprocessingEfficiency = db.Column(db.Float)
    reprocessingStationsTake = db.Column(db.Float)
    reprocessingHangarFlag = db.Column(db.Integer)

    def __init__(self,stationID,security,dockingCostPerVolume,maxShipVolumeDockable,officeRentalCost,operationID,stationTypeID,corporationID,solarSystemID,constellationID,regionID,stationName,x,y,z,reprocessingEfficiency,reprocessingStationsTake,reprocessingHangarFlag):
        self.stationID = stationID
        self.security = security
        self.dockingCostPerVolume = dockingCostPerVolume
        self.maxShipVolumeDockable = maxShipVolumeDockable
        self.officeRentalCost = officeRentalCost
        self.operationID = operationID
        self.stationTypeID = stationTypeID
        self.corporationID = corporationID
        self.solarSystemID = solarSystemID
        self.constellationID = constellationID
        self.regionID = regionID
        self.stationName = stationName
        self.x = x
        self.y = y
        self.z = z
        self.reprocessingEfficiency = reprocessingEfficiency
        self.reprocessingStationsTake = reprocessingStationsTake
        self.reprocessingHangarFlag = reprocessingHangarFlag

    def __repr__(self):
        return '<stationID %r>' % self.stationID