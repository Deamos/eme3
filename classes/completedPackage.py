import uuid
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class completedPackage(db.Model):
    __tablename__ = 'completedPackage'
    id = db.Column(db.Integer, primary_key=True)
    packageID = db.Column(db.String(64),unique=True)
    charID = db.Column(db.BigInteger)
    blueprintTypeID = db.Column(db.Integer)
    blueprintME = db.Column(db.Integer)
    blueprintTE = db.Column(db.Integer)
    runs = db.Column(db.Integer)
    producedAmount = db.Column(db.Integer)
    typeIDProduced = db.Column(db.Integer)
    sysID = db.Column(db.Integer)
    sysIndex = db.Column(db.Float)
    facilityMEModifier = db.Column(db.Float)
    facilityTEModifier = db.Column(db.Float)
    tax = db.Column(db.Float)
    sysBuildCosts = db.Column(db.Float)
    componentCost = db.Column(db.Float)
    producedItemPrice = db.Column(db.Float)
    profit = db.Column(db.Float)
    startDate = db.Column(db.DateTime)
    endDate = db.Column(db.DateTime)

    def __init__(self,packageID,charID,blueprintTypeID,blueprintME,blueprintTE,runs,producedAmount,typeIDProduced,sysID,sysIndex,facilityMEModifier,facilityTEModifier,tax,sysBuildCosts,componentCost,producedItemPrice,profit,startDate,endDate):

        self.packageID = packageID
        self.charID = long(charID)
        self.blueprintTypeID = blueprintTypeID
        self.blueprintME = blueprintME
        self.blueprintTE = blueprintTE
        self.runs = runs
        self.producedAmount = producedAmount
        self.typeIDProduced = typeIDProduced
        self.sysID = sysID
        self.sysIndex = float(sysIndex)
        self.facilityMEModifier = float(facilityMEModifier)
        self.facilityTEModifier = float(facilityTEModifier)
        self.tax = float(tax)
        self.sysBuildCosts = float(sysBuildCosts)
        self.componentCost = float(componentCost)
        self.producedItemPrice = float(producedItemPrice)
        self.profit = float(profit)
        self.startDate = startDate
        self.endDate = endDate

    def __repr__(self):
        return '<packageID %r>' % self.packageID