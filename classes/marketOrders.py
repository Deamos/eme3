import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class marketOrders(db.Model):
    __tablename__="marketOrders"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.Integer)
    type = db.Column(db.String(4))
    orderID = db.Column(db.BigInteger)
    placingCharID = db.Column(db.BigInteger)
    stationID = db.Column(db.Integer)
    volEntered = db.Column(db.Integer)
    volRemaining = db.Column(db.Integer)
    minVolume = db.Column(db.Integer)
    orderState = db.Column(db.Integer)
    typeID = db.Column(db.Integer)
    range = db.Column(db.Integer)
    accountKey = db.Column(db.Integer)
    duration = db.Column(db.Integer)
    escrow = db.Column(db.Float)
    price = db.Column(db.Float)
    bid = db.Column(db.Boolean)
    issued = db.Column(db.DateTime)
    cachedUntil = db.Column(db.DateTime)

    def __init__(self, charID, type, orderID, placingCharID, stationID, volEntered, volRemaining, minVolume, orderState, typeID, range, accountKey, duration, escrow, price, bid, issued, cachedUntil):
        self.charID = int(charID)
        self.type = type
        self.orderID = long(orderID)
        self.placingCharID = long(placingCharID)
        self.stationID = int(stationID)
        self.volEntered = int(volEntered)
        self.volRemaining = int(volRemaining)
        self.minVolume = int(minVolume)
        self.orderState = int(orderState)
        self.typeID = int(typeID)
        self.range = int(range)
        self.accountKey = int(accountKey)
        self.duration = int(duration)
        self.escrow = float(escrow)
        self.price = float(price)
        self.bid = bid
        self.issued = issued
        self.cachedUntil = cachedUntil

    def __repr__(self):
        return '<id %r>' % self.id

    def is_valid(self):
        if self.cachedUntil > datetime.datetime.utcnow():
            return True
        else:
            return False