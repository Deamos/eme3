import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class packageMaterials(db.Model):
    __tablename__ = "packageMaterials"
    id = db.Column(db.Integer, primary_key=True)
    packageID = db.Column(db.String(64))
    typeID = db.Column(db.Integer)
    quantity = db.Column(db.Integer)
    ppu = db.Column(db.Float)

    def __init__(self,packageID,typeID,quantity,ppu):
        self.packageID = packageID
        self.typeID = typeID
        self.quantity = quantity
        self.ppu = ppu

    def __repr__(self):
        return '<id %r>' % self.id
