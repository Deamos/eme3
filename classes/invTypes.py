import re
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class invTypes(db.Model):
    __tablename__="invTypes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    mass = db.Column(db.Float)
    volume = db.Column(db.Float)
    portionSize = db.Column(db.Integer)

    def __init__(self, typeID, typeName, mass, volume, portionSize):
        self.id = typeID
        self.name = str(re.sub(r'[^\x00-\x7F]+','',typeName)).decode('utf-8')
        self.mass = mass
        self.volume = volume
        self.portionSize = portionSize

    def __repr__(self):
        return '<id %r>' % self.id