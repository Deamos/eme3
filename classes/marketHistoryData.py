import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class marketHistoryData(db.Model):
    __tablename__="marketHistoryData"
    id = db.Column(db.Integer, primary_key=True)
    typeID = db.Column(db.Integer)
    regionID = db.Column(db.Integer)
    date = db.Column(db.DateTime)
    highPrice = db.Column(db.Float)
    avgPrice = db.Column(db.Float)
    lowPrice = db.Column(db.Float)
    volume = db.Column(db.Integer)
    orderCount = db.Column(db.Integer)

    def __init__(self, typeID, regionID, date, highPrice, avgPrice, lowPrice, volume, orderCount):
        self.typeID = typeID
        self.regionID = regionID
        self.date = date
        self.highPrice = highPrice
        self.avgPrice = avgPrice
        self.lowPrice = lowPrice
        self.volume = volume
        self.orderCount = orderCount

    def __repr__(self):
        return '<id %r>' % self.id