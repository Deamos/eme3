import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class linkedMarket(db.Model):
    __tablename__="linkedMarket"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    marketID = db.Column(db.BigInteger, unique=True)
    linkedPackage = db.Column(db.String(64),db.ForeignKey('package.packageID'))

    def __init__(self,charID,marketID,linkedPackage):
        self.charID = long(charID)
        self.marketID = long(marketID)
        self.linkedPackage = str(linkedPackage)

    def __repr__(self):
        return '<id %r>' % self.id
