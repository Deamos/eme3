import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class pilot(db.Model):
    __tablename__ = 'pilot'
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    accessToken = db.Column(db.String(128))
    tokenType = db.Column(db.String(64))
    expires = db.Column(db.DateTime)
    refreshToken = db.Column(db.String(128))
    defaultBuildLocation = db.Column(db.Integer)
    defaultPriceCalcType = db.Column(db.String(4))
    defaultPriceCalcHighLow = db.Column(db.String(4))
    defaultPriceLocationType = db.Column(db.Integer)
    defaultPriceLocationID = db.Column(db.Integer)


    def __init__(self,charID,accessToken,tokenType,refreshToken,expires_in):
        self.charID = long(charID)
        self.accessToken = accessToken
        self.tokenType = tokenType
        self.refreshToken = refreshToken
        self.expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=expires_in)
        self.defaultPriceCalcType = 'sell'
        self.defaultPriceCalcHighLow = 'low'
        self.defaultPriceLocationType = 1
        self.defaultPriceLocationID = 30000142
        self.defaultBuildLocation = 30000145

    def __repr__(self):
        return '<charID %r>' % self.charID

    def updateToken(self,accessToken, refreshToken, expires_in):
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=expires_in)

    def is_valid(self):
        if self.expires < datetime.datetime.utcnow():
            return False
        else:
            return True