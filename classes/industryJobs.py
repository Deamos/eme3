import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db
class industryJobs(db.Model):
    __tablename__="industryJobs"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    type = db.Column(db.String(4))
    jobID = db.Column(db.BigInteger)
    installerID = db.Column(db.BigInteger)
    facilityID = db.Column(db.Integer)
    solarSystemID = db.Column(db.Integer)
    stationID = db.Column(db.Integer)
    activityID = db.Column(db.Integer)
    blueprintID = db.Column(db.BigInteger)
    blueprintTypeID = db.Column(db.Integer)
    blueprintLocationID = db.Column(db.Integer)
    outputLocationID = db.Column(db.Integer)
    runs = db.Column(db.Integer)
    cost = db.Column(db.Float)
    licensedRuns = db.Column(db.Integer)
    probability = db.Column(db.Float)
    productTypeID = db.Column(db.Integer)
    status = db.Column(db.Integer)
    timeInSeconds = db.Column(db.Integer)
    startDate = db.Column(db.DateTime)
    endDate = db.Column(db.DateTime)
    pauseDate = db.Column(db.DateTime)
    completedDate = db.Column(db.DateTime)
    completedCharacterID = db.Column(db.BigInteger)
    successfulRuns = db.Column(db.Integer)
    cachedUntil = db.Column(db.DateTime)

    def __init__(self, charID, type, jobID, installerID, facilityID, solarSystemID, stationID, activityID, blueprintID, blueprintTypeID, blueprintLocationID, outputLocationID, runs, cost, licensedRuns, probability, productTypeID, status, timeInSeconds, startDate, endDate, pauseDate, completedDate, completedCharacterID, successfulRuns, cachedUntil):
        self.charID = long(charID)
        self.type = type
        self.jobID = long(jobID)
        self.installerID = long(installerID)
        self.facilityID = int(facilityID)
        self.solarSystemID = int(solarSystemID)
        self.stationID = int(stationID)
        self.activityID = int(activityID)
        self.blueprintID = long(blueprintID)
        self.blueprintTypeID = int(blueprintTypeID)
        self.blueprintLocationID = int(blueprintLocationID)
        self.outputLocationID = int(outputLocationID)
        self.runs = int(runs)
        self.cost = float(cost)
        self.licensedRuns = int(licensedRuns)
        self.probability = float(probability)
        self.productTypeID = int(productTypeID)
        self.status = int(status)
        self.timeInSeconds = int(timeInSeconds)
        self.startDate = startDate
        self.endDate = endDate
        self.pauseDate = pauseDate
        self.completedDate = completedDate
        self.completedCharacterID = long(completedCharacterID)
        self.successfulRuns = int(successfulRuns)
        self.cachedUntil = cachedUntil

    def __repr__(self):
        return '<id %r>' % self.id

    def is_valid(self):
        if self.cachedUntil > datetime.datetime.utcnow():
            return True
        else:
            return False