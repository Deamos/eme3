import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class mapSolarSystems(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    regionID = db.Column(db.Integer)
    constellationID = db.Column(db.Integer)
    solarSystemID = db.Column(db.Integer,unique=True)
    solarSystemName = db.Column(db.String(64),unique=True)
    x = db.Column(db.Float)
    y = db.Column(db.Float)
    z = db.Column(db.Float)
    xMin = db.Column(db.Float)
    xMax = db.Column(db.Float)
    yMin = db.Column(db.Float)
    yMax = db.Column(db.Float)
    zMin = db.Column(db.Float)
    zMax = db.Column(db.Float)
    luminosity = db.Column(db.Float)
    border = db.Column(db.Float)
    fringe = db.Column(db.Float)
    corridor = db.Column(db.Float)
    hub = db.Column(db.Float)
    international = db.Column(db.Float)
    regional = db.Column(db.Float)
    constellation = db.Column(db.Float)
    security = db.Column(db.Float)
    factionID = db.Column(db.Integer)
    radius = db.Column(db.Float)
    sunTypeID = db.Column(db.Integer)
    securityClass = db.Column(db.String(8))

    def __init__(self, regionID,constellationID,solarSystemID,solarSystemName,x,y,z,xMin,xMax,yMin,yMax,zMin,zMax,luminosity,border,fringe,corridor,hub,international,regional,constellation,security,factionID,radius,sunTypeID,securityClass):
        self.regionID = regionID
        self.constellationID = constellationID
        self.solarSystemID = solarSystemID
        self.solarSystemName = solarSystemName
        self.x = x
        self.y = y
        self.z = z
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        self.zMin = zMin
        self.zMax = zMax
        self.luminosity = luminosity
        self.border = border
        self.fringe = fringe
        self.corridor = corridor
        self.hub = hub
        self.international = international
        self.regional = regional
        self.constellation = constellation
        self.security = security
        self.factionID = factionID
        self.radius = radius
        self.sunTypeID = sunTypeID
        self.securityClass = securityClass

    def __repr__(self):
        return '<solarSystemID %r>' % self.solarSystemID