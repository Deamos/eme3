import os

basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class mapRegions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    regionID = db.Column(db.Integer, unique=True)
    regionName = db.Column(db.String(32), unique=True)
    x = db.Column(db.Float)
    y = db.Column(db.Float)
    z = db.Column(db.Float)
    xMin = db.Column(db.Float)
    xMax = db.Column(db.Float)
    yMin = db.Column(db.Float)
    yMax = db.Column(db.Float)
    zMin = db.Column(db.Float)
    zMax = db.Column(db.Float)
    factionID = db.Column(db.Integer)
    radius = db.Column(db.Float)


    def __init__(self, regionID,regionName,x,y,z,xMin,xMax,yMin,yMax,zMin,zMax,factionID,radius):
        self.regionID = regionID
        self.regionName = regionName
        self.x = x
        self.y = y
        self.z = z
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        self.zMin = zMin
        self.zMax = zMax
        self.factionID = factionID
        self.radius = radius

    def __repr__(self):
        return '<regionID %r>' % self.regionID