import uuid
import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class package(db.Model):
    __tablename__ = 'package'
    id = db.Column(db.Integer, primary_key=True)
    packageID = db.Column(db.String(64),unique=True)
    charID = db.Column(db.BigInteger)
    blueprintDataID = db.Column(db.Integer)
    ownedBPID = db.Column(db.BigInteger,db.ForeignKey('blueprints.itemID'))
    runs = db.Column(db.Integer)
    producedAmount = db.Column(db.Integer)
    timeEfficiency = db.Column(db.BigInteger)
    materialEfficiency = db.Column(db.BigInteger)
    typeIDProduced = db.Column(db.Integer)
    sysID = db.Column(db.Integer)
    sysIndex = db.Column(db.Float)
    facilityMEModifier = db.Column(db.Float)
    facilityTEModifier = db.Column(db.Float)
    tax = db.Column(db.Float)
    sysBuildCosts = db.Column(db.Float)
    componentCost = db.Column(db.Float)
    producedItemPrice = db.Column(db.Float)
    salesTax = db.Column(db.Float)
    expectedProfit = db.Column(db.Float)
    stage = db.Column(db.Integer)
    startDate = db.Column(db.DateTime)
    linkedJobs = db.relationship('linkedManufacturing', backref='packages', lazy="joined")
    linkedMarket = db.relationship('linkedMarket',backref='packages',lazy="joined")

    def __init__(self,charID,blueprintDataID,ownedBPID,runs,producedAmount,timeEfficiency,materialEfficiency,typeIDProduced,sysID,sysIndex,facilityMEModifier,facilityTEModifier,tax,sysBuildCosts,componentCost,producedItemPrice,expectedProfit,salesTax):

        self.packageID = str(uuid.uuid4())
        self.charID = long(charID)
        self.blueprintDataID = blueprintDataID
        self.ownedBPID = ownedBPID
        self.runs = runs
        self.producedAmount = producedAmount
        self.timeEfficiency = timeEfficiency
        self.materialEfficiency = materialEfficiency
        self.typeIDProduced = typeIDProduced
        self.sysID = sysID
        self.sysIndex = float(sysIndex)
        self.facilityMEModifier = float(facilityMEModifier)
        self.facilityTEModifier = float(facilityTEModifier)
        self.tax = float(tax)
        self.sysBuildCosts = float(sysBuildCosts)
        self.componentCost = float(componentCost)
        self.producedItemPrice = float(producedItemPrice)
        self.expectedProfit = float(expectedProfit)
        self.salesTax = float(salesTax)
        self.stage = 0
        self.startDate = datetime.datetime.utcnow()

    def __repr__(self):
        return '<packageID %r>' % self.packageID