import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class priceCache(db.Model):
    __tablename__ = "priceCache"
    id = db.Column(db.Integer, primary_key=True)
    typeID = db.Column(db.Integer)
    type = db.Column(db.String(12))
    locationID = db.Column(db.BigInteger)
    buyLow = db.Column(db.Float)
    buyHigh = db.Column(db.Float)
    sellLow = db.Column(db.Float)
    sellHigh = db.Column(db.Float)
    cachedUntil = db.Column(db.DateTime)

    def __init__(self,typeID,type,locationID,buyLow,buyHigh,sellLow,sellHigh):
        self.typeID = typeID
        self.type = type
        self.locationID = locationID
        self.buyLow = buyLow
        self.buyHigh = buyHigh
        self.sellLow = sellLow
        self.sellHigh = sellHigh
        self.cachedUntil = datetime.datetime.utcnow() + datetime.timedelta(hours=6)

    def __repr__(self):
        return '<id %r>' % self.id

    def is_valid(self):
        if self.cachedUntil > datetime.datetime.utcnow():
            return True
        else:
            return False