import datetime
import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class knownCitadel(db.Model):
    __tablename__ = "knownCitadel"
    id = db.Column(db.Integer, primary_key=True)
    citadelID = db.Column(db.BigInteger, unique=True)
    typeID = db.Column(db.Integer)
    regionID = db.Column(db.Integer)
    name = db.Column(db.String(256))
    solarSystemID = db.Column(db.Integer)
    lastSeen = db.Column(db.DateTime)

    def __init__(self,citadelID,typeID,regionID,solarSystemID,name):
        self.citadelID = citadelID
        self.typeID = typeID
        self.regionID = regionID
        self.solarSystemID = solarSystemID
        self.name = name
        self.lastSeen = datetime.datetime.utcnow()

    def __repr__(self):
        return '<citadelID %r>' % self.citadelID