import datetime
import os

basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class assetList(db.Model):
    __tablename__="assetList"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    type = db.Column(db.String(4))
    itemID = db.Column(db.BigInteger)
    locationID = db.Column(db.BigInteger)
    typeID = db.Column(db.Integer)
    quantity = db.Column(db.BigInteger)
    flag = db.Column(db.Integer)
    singleton = db.Column(db.Boolean)
    rawQuantity = db.Column(db.BigInteger)
    cachedUntil = db.Column(db.DateTime)

    def __init__(self, charID, type, itemID, locationID, typeID, quantity, flag, singleton, rawQuantity, cachedUntil):
        self.charID = long(charID)
        self.type = type
        self.itemID = long(itemID)
        self.locationID = long(locationID)
        self.typeID = int(typeID)
        self.quantity = long(quantity)
        self.flag = int(flag)
        self.singleton = singleton
        self.rawQuantity = long(rawQuantity)
        self.cachedUntil = cachedUntil

    def __repr__(self):
        return '<id %r>' % self.id

    def is_valid(self):
        if self.cachedUntil > datetime.datetime.utcnow():
            return True
        else:
            return False
