import datetime
import os

basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class accountBalance(db.Model):
    __tablename__="accountBalance"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    type = db.Column(db.String(4))
    accountID = db.Column(db.Integer)
    accountKey = db.Column(db.Integer)
    balance = db.Column(db.Float)
    cachedUntil = db.Column(db.DateTime)

    def __init__(self, charID, type, accountID, accountKey, balance, cachedUntil):
        self.charID = long(charID)
        self.type = type
        self.accountID = int(accountID)
        self.accountKey = int(accountKey)
        self.balance = float(balance)
        self.cachedUntil = cachedUntil

    def __repr__(self):
        return '<id %r>' % self.id

    def is_valid(self):
        if self.cachedUntil > datetime.datetime.utcnow():
            return True
        else:
            return False
