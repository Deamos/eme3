import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db

class badTypeIDs(db.Model):
    __tablename__="badTypeIDs"
    id = db.Column(db.Integer, primary_key=True)
    typeID = db.Column(db.Integer)

    def __init__(self,typeID):
        self.typeID = int(typeID)

    def __repr__(self):
        return '<id %r>' % self.id
