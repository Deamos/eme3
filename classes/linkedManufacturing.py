import os
basedir = os.path.abspath(os.path.dirname(__file__))
from app import db


class linkedManufacturing(db.Model):
    __tablename__="linkedManufacturing"
    id = db.Column(db.Integer, primary_key=True)
    charID = db.Column(db.BigInteger)
    industryJobID = db.Column(db.BigInteger, unique=True)
    linkedPackage = db.Column(db.String(64),db.ForeignKey('package.packageID'))

    def __init__(self,charID,industryJobID,linkedPackage):
        self.charID = long(charID)
        self.industryJobID = long(industryJobID)
        self.linkedPackage = str(linkedPackage)

    def __repr__(self):
        return '<id %r>' % self.id
